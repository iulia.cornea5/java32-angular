import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserItemComponent } from './user-item/user-item.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';

// http://localhost:4200/register-user
const routes: Routes = [
  {path: "register-user", component: UserRegistrationComponent},
  {path: "user", component: UserItemComponent},
  {path: "users", component: UserListComponent},
  {path: "login", component: UserLoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

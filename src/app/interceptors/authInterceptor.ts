import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from '../user.service'

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    
    constructor (private userService: UserService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const currentUser = this.userService.getUser();
        if(!!currentUser) {
            // Authorization Basic enc(user:pass)    
            const requestWithAuth = req.clone({
                headers: req.headers.set(
                    'Authorization', 
                    'Basic ' + btoa(currentUser.userName + ":" + currentUser.password))
            })
            return next.handle(requestWithAuth);
        } else {
           // Authorization Basic enc(user:pass)    
            const requestWithAuth = req.clone({
                headers: req.headers.set(
                    'Authorization', 
                    '')
            })
            return next.handle(requestWithAuth);
        }

    }

}
import { Component, OnInit } from '@angular/core';

import { UserService } from 'src/app/user.service';
import { UserCreateDTO } from '../model/userCreateDTO';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {

  username = "";

  password = "";

    constructor(private service: UserService) {   }

  ngOnInit(): void {
  }

  onClick() {
    alert(this.username + " " + this.password);

  this.service.createUser(new UserCreateDTO(this.username, this.password)).subscribe(response => {
    console.log(response);
  })
  }

}

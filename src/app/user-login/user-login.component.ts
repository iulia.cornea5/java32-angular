import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserCreateDTO } from '../model/userCreateDTO';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  username = "";

  password = "";

  constructor(private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
  }

  onClick() {
    this.userService.login(new UserCreateDTO(this.username, this.password));
    this.router.navigate(['/users']);
  }

}

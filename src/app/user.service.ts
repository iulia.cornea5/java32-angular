import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserCreateDTO } from 'src/app/model/userCreateDTO';
import { UserDTO } from './model/userDTO';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userLabel = "appUser";

  httpClient: HttpClient;

  constructor(client: HttpClient) {
    this.httpClient = client;
  }

  createUser(createUserDTO: UserCreateDTO): Observable<any> {
    return this.httpClient.post("api/users/signup", createUserDTO);
  }

  getAll(): Observable<any> {
    return this.httpClient.get("api/users");
  }

  login(userWithCredentials: UserCreateDTO) {
    this.httpClient.post("api/users/login", userWithCredentials).subscribe(
      response => {
        const responseUser = response as UserDTO;
        const appUser: UserDTO = {
          id: responseUser.id,
          userName: responseUser.userName,
          password: userWithCredentials.password,
          profilePicture: ''
        }
        this.setUser(appUser);
      }

    )
  }

  logout() {
    localStorage.setItem(this.userLabel, '');
  }

  private setUser(user: UserDTO) {
    localStorage.setItem(this.userLabel, JSON.stringify(user));
  }

  public getUser(): UserDTO {
    if (!localStorage.getItem(this.userLabel))
      return null;
    else
      return JSON.parse(localStorage.getItem(this.userLabel));
  }
}

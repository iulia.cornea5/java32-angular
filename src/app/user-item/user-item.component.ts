import { Component, Input, OnInit } from '@angular/core';
import { UserDTO } from 'src/app/model/userDTO';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent implements OnInit {


  @Input()
  user: UserDTO = {
    id: null,
    userName: "",
    profilePicture: "",
    password: ""
  }

  constructor() { }

  ngOnInit(): void {

  }

}
